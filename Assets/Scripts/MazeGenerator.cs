﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class cell
{
    public bool isCellChecked;
    public GameObject North;
    public GameObject East;
    public GameObject West;
    public GameObject South;

    public cell()
    {
        North = null;
        East = null;
        West = null;
        South = null;
    }

}

public class MazeGenerator : MonoBehaviour
{
    // Start is called before the first frame update
    #region public vars

    public GameObject CellFloor;
    public GameObject MazeParent;
    public GameObject Wall;
    private float wallLength = 1.0f;
    public int xSize = 5;//how many rows
    public int ySize = 5;//how many columns

    #endregion

    #region private vars
    private int currentCell = 0;//
    private Vector3 startPos;
    private GameObject tempPrefab;
    private cell[] cells;
    private int totalCells;
    private int visitedCells;
    private bool startedCreating;
    private int currentNeighbour;// the random cell ;
    private int backUpCell;
    private List<int> pendingCells;
    private int wallToBreak;
    private GameObject floorHolder;
    #endregion
    void Start()
    {
        GenerateWalls();

    }
    //we will traverse through the for loop rows and colums to create Grid
    /// <summary>
    /// this method create the grid system(of given size rowxcol) without any pathways between them
    /// </summary>
    void GenerateWalls()
    {
        backUpCell = 0;
        visitedCells = 0;
        pendingCells = new List<int>();
        pendingCells.Clear();
        totalCells = xSize * ySize;
        cells = new cell[totalCells];
        startPos = new Vector3((-xSize / 2) + wallLength / 2, wallLength / 2, (-ySize / 2) + wallLength);
        Vector3 tempPos = startPos;

        //Create floor for each cell
        GameObject floorObject;
        floorHolder = new GameObject();
        floorHolder.name = "FloorHolder";

        //wall for x axis
        for (int j = 0; j < ySize; j++)
        {
            for (int i = 0; i <= xSize; i++)
            {
                tempPos = new Vector3(startPos.x + (i * wallLength) - wallLength / 2.0f, 0.5f, startPos.z + (j * wallLength) - wallLength / 2);
                tempPrefab = Instantiate(Wall, tempPos, Quaternion.identity) as GameObject;
                tempPrefab.transform.parent = MazeParent.transform;

                //====here we are creating the floor respective to each cell, it helps us for the movement from one cell to another cell,(Instead of creating a whole floor we created cell wise floor)
                if (j < ySize && i < xSize && CellFloor)
                {
                    floorObject = Instantiate(CellFloor, new Vector3(tempPos.x + .5f, 0f, tempPos.z), Quaternion.identity) as GameObject;
                    floorObject.transform.parent = floorHolder.transform;
                }
            }
        }
        //wall for y axis
        for (int l = 0; l <= ySize; l++)
        {
            for (int k = 0; k < xSize; k++)
            {
                tempPos = new Vector3(startPos.x + (k * wallLength), 0.5f, startPos.z + (l * wallLength) - wallLength);
                tempPrefab = Instantiate(Wall, tempPos, Quaternion.Euler(0f, 90, 0.0f)) as GameObject;
                tempPrefab.transform.parent = MazeParent.transform;

            }
        }
        GenerateCells();
    }

    void GenerateCells()
    {
        GameObject[] mazeElements;

        int childCount = MazeParent.transform.childCount;

        mazeElements = new GameObject[childCount];

        int ewDirection = 0;
        int childProgress = 0;
        int tempCount = 0;

        for (int index = 0; index < childCount; ++index)
        {
            mazeElements[index] = MazeParent.transform.GetChild(index).gameObject;
        }
        // now assining the walls to the cell side like S,N,E,W
        for (int cellIndex = 0; cellIndex < cells.Length; cellIndex++)
        {
            if (tempCount == xSize)
            {
                ewDirection++;
                tempCount = 0;
            }

            cells[cellIndex] = new cell();
            cells[cellIndex].South = mazeElements[childProgress + (xSize + 1) * ySize];
            cells[cellIndex].East = mazeElements[ewDirection];

            ewDirection++;
            tempCount++;
            childProgress++;

            // Assign the  w,s cell walls
            cells[cellIndex].West = mazeElements[ewDirection];
            cells[cellIndex].North = mazeElements[(childProgress + (xSize + 1) * ySize) + xSize - 1];
        }

        GenerateMaze();
    }

    void GenerateMaze()
    {
        while (visitedCells < totalCells)
        {
            if (startedCreating)
            {
                FindNeighbours();
                if (cells[currentNeighbour].isCellChecked == false && cells[currentCell].isCellChecked == true)
                {
                    //create space here between the walls

                    GenerateWallSpace();

                    //End of space creation
                    cells[currentNeighbour].isCellChecked = true;
                    visitedCells++;
                    pendingCells.Add(currentCell);
                    currentCell = currentNeighbour;
                    if (pendingCells.Count > 0)
                    {
                        backUpCell = pendingCells.Count - 1;

                    }
                }
            }
            if (!startedCreating)
            {
                currentCell = UnityEngine.Random.Range(0, cells.Length);
                startedCreating = true;
                cells[currentCell].isCellChecked = true;
                visitedCells++;

            }

            if (transform.GetComponent<MazePathFinder>())
            {
                transform.GetComponent<MazePathFinder>().ActivatePathFinder(xSize, ySize, cells);
            }

        }

        StartAndExistDoors();
    }

    void StartAndExistDoors()
    {
        int allMazeElements = MazeParent.transform.childCount;
        MazeParent.transform.GetChild(0).gameObject.SetActive(false);
        MazeParent.transform.GetChild(allMazeElements - 1).gameObject.SetActive(false);
    }

    void FindNeighbours()
    {

        int length = 0;
        int[] neighbours = new int[4];//s,n,e,w
        int[] connectedWalls = new int[4];//s,n,e,w
        int check = 0;
        check = ((currentCell + 1) / xSize);
        check -= 1;
        check *= xSize;
        check += xSize;
        //for W
        if (currentCell + 1 < totalCells && (currentCell + 1) != check && xSize > 1)
        {
            if (!cells[currentCell + 1].isCellChecked)//add to the neighbours
            {
                neighbours[length] = currentCell + 1;
                connectedWalls[length] = 3;
                length++;

            }
        }
        //for E
        if (currentCell - 1 >= 0 && currentCell != check && xSize > 1)
        {
            if (!cells[currentCell - 1].isCellChecked)//add to the neighbours
            {
                neighbours[length] = currentCell - 1;
                connectedWalls[length] = 2;
                length++;

            }
        }
        //for N
        if (currentCell + xSize < totalCells && ySize > 1)
        {
            if (!cells[currentCell + xSize].isCellChecked)//add to the neighbours
            {
                neighbours[length] = currentCell + xSize;
                connectedWalls[length] = 1;
                length++;

            }
        }
        //for S
        if (currentCell - xSize >= 0 && ySize > 1)
        {
            if (!cells[currentCell - xSize].isCellChecked)//add to the neighbours
            {
                neighbours[length] = currentCell - xSize;
                connectedWalls[length] = 4;
                length++;

            }
        }
        if (length != 0)
        {
            int randomCell = Random.Range(0, length);
            currentNeighbour = neighbours[randomCell];
            wallToBreak = connectedWalls[randomCell];
        }
        else
        {
            if (backUpCell > 0)
            {
                currentCell = pendingCells[backUpCell];
                backUpCell--;
            }
        }

    }

    /// <summary>
    /// breaking the walls to create paths
    /// </summary>
    void GenerateWallSpace()
    {
        switch (wallToBreak)
        {
            case 1:
                Destroy(cells[currentCell].North);
                break;
            case 2:
                Destroy(cells[currentCell].East);
                break;
            case 3:
                Destroy(cells[currentCell].West);
                break;
            case 4:
                Destroy(cells[currentCell].South);
                break;

        }

    }
}
