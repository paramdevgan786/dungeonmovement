﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    // Start is called before the first frame update
    #region public vars
    [HideInInspector]
    public int currentTarget = 0;
    [HideInInspector]
    public int startPoint = 0;
    [HideInInspector]
    public Transform currentFloor;
    #endregion

    #region private vars
    private List<int> path;
    private int lastTarget = 0;
    private GameObject floorHolder;
    private int currentPoint = 0;
    private MazePathFinder mazePathFinder;
    #endregion
    void Start()
    {
        mazePathFinder = FindObjectOfType<MazePathFinder>();
        path = mazePathFinder.FindPath(startPoint, currentTarget);

    }

    // Update is called once per frame
    void Update()
    {
        if (!floorHolder)
        {
            floorHolder = GameObject.Find("FloorHolder");
            return;
        }
        if (lastTarget != currentTarget)
        {
            path = new List<int>();
            path.Clear();
            path = mazePathFinder.FindPath(startPoint, currentTarget);
            lastTarget = currentTarget;
            currentPoint = 0;
        }
        else
        {

            if (currentPoint < path.Count)
            {
                transform.position = Vector3.MoveTowards(transform.position, floorHolder.transform.GetChild(path[currentPoint]).transform.position, 0.05f);
                if ((transform.position - floorHolder.transform.GetChild(path[currentPoint]).transform.position).magnitude <= 0.25f)
                {
                    currentPoint++;
                }
            }
        }
    }

    void OnCollisionStay(Collision collision)
    {
        if (collision.collider.gameObject.tag.Contains("Floor"))
        {
            currentFloor = collision.collider.transform;
        }
    }

    public void ChangeRoute(Transform endTarget)
    {

        Debug.Log("got the destination");
        for (int i = 0; i < floorHolder.transform.childCount; i++)
        {
            if (currentFloor == floorHolder.transform.GetChild(i).transform)
            {
                startPoint = i;
                break;
            }
        }

        for (int i = 0; i < floorHolder.transform.childCount; i++)
        {
            if (endTarget == floorHolder.transform.GetChild(i).transform)
            {
                currentTarget = i;
                break;
            }
        }
    }
}
