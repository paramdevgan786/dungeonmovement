﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This script is being attached to the floor holder at run time, once player gets created we destroy this sscript
/// </summary>
public class PlayerSpawner : MonoBehaviour
{
    // Start is called before the first frame update
    [HideInInspector]
    public int spawnInCell = 0;//this is in which cell you want your player to spawn

    //here we are checking if the floor holder is having enough cell floor so that player can move
    // Update is called once per frame
    void Update()
    {
        if (transform.childCount > 0)
        {
            Vector3 pos = new Vector3(transform.GetChild(spawnInCell).transform.position.x, transform.GetChild(spawnInCell).transform.position.y + .5f,
                transform.GetChild(spawnInCell).transform.position.z);
            Instantiate(Resources.Load("Player"), pos, Quaternion.identity);
            Destroy(transform.GetComponent<PlayerSpawner>());

        }
    }
}
