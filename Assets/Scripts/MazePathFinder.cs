﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MazePathFinder : MonoBehaviour
{
    //Same as maze parameters
    private int xSize = 0;
    private int ySize = 0;
    private List<int> path;
    private bool canFind = false;
    private cell[] myMaze;

    //activate the path finder atleast once with correct values

    public void ActivatePathFinder(int passedSizeX, int passedSizeY, cell[] passedMaze)
    {
        xSize = passedSizeX;
        ySize = passedSizeY;
        myMaze = passedMaze;
        canFind = true;
        Debug.Log("Maze Path Finder is Ready to use");
    }

    //Finds the path and returns as a List<int>
    public List<int> FindPath(int passedStart, int passedEnd)
    {
        if (!canFind)
        {
            Debug.LogWarning("not ready");
            return null;
        }


        if (passedStart < 0 || passedStart >= (xSize * ySize))
        {
            Debug.LogWarning("start is not there");
            return null;
        }

        if (passedEnd < 0 || passedEnd >= (xSize * ySize))
        {
            Debug.LogWarning("end is not there");
            return null;
        }

        foreach (cell currentMaze in myMaze)
        {
            currentMaze.isCellChecked = false;
        }

        path = new List<int>();
        path.Clear();

        int currentPoint = passedStart;
        myMaze[currentPoint].isCellChecked = true;
        path.Add(currentPoint);

        while (path.Count < (xSize * ySize) && path.Count > 0)
        {
            currentPoint = path[path.Count - 1];
            if (currentPoint == passedEnd)
            {
                break;
            }
            GiveMeNieghbour(currentPoint);
        }

        Debug.Log("Path Found");

        return path;
    }

    //Gives a unvisited and accesible neighbour 
    void GiveMeNieghbour(int currentCell)
    {
        int length = 0;
        int[] neighbour = new int[4];
        int check = 0;
        int totalCells = xSize * ySize;
        check = ((currentCell + 1) / xSize);
        check -= 1;
        check *= xSize;
        check += xSize;

        //west
        if (!myMaze[currentCell].West && currentCell + 1 < totalCells && (currentCell + 1) != check && xSize > 1)
        {
            if (myMaze[currentCell + 1].isCellChecked == false)
            {
                neighbour[length] = currentCell + 1;
                length++;
            }
        }
        //east
        if (!myMaze[currentCell].East && currentCell - 1 >= 0 && currentCell != check && xSize > 1)
        {
            if (myMaze[currentCell - 1].isCellChecked == false)
            {
                neighbour[length] = currentCell - 1;
                length++;
            }
        }
        //north
        if (!myMaze[currentCell].North && currentCell + xSize < totalCells && ySize > 1)
        {
            if (myMaze[currentCell + xSize].isCellChecked == false)
            {
                neighbour[length] = currentCell + xSize;
                length++;
            }
        }
        //south
        if (!myMaze[currentCell].South && currentCell - xSize >= 0 && ySize > 1)
        {
            if (myMaze[currentCell - xSize].isCellChecked == false)
            {
                neighbour[length] = currentCell - xSize;
                length++;
            }
        }
        if (length != 0)
        {
            int theChosenOne = Random.Range(0, length);
            myMaze[neighbour[theChosenOne]].isCellChecked = true;
            path.Add(neighbour[theChosenOne]);
        }
        else
        {
            if (path.Count > 0)
            {
                path.RemoveAt(path.Count - 1);
            }

        }

    }


}
