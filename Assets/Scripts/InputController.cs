﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    private PlayerMovementController playerMovement;
    private bool isSpawnerAttached;

    // Update is called once per frame
    void Update()
    {
        if (!playerMovement)
        {
            if (GameObject.Find("FloorHolder") && !isSpawnerAttached)
            {
                GameObject.Find("FloorHolder").AddComponent<PlayerSpawner>();
                isSpawnerAttached = true;
            }
            playerMovement = FindObjectOfType<PlayerMovementController>();
        }

        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit = new RaycastHit();
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100f))
            {
                if (hit.transform.tag == "CellFloor")
                {
                    hit.transform.GetComponent<Renderer>().material.color = Color.red;
                    if (playerMovement)
                    {
                        playerMovement.ChangeRoute(hit.transform);
                    }
                }
            }
        }
    }
}
